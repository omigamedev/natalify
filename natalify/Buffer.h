#ifndef __natalify__Buffer__
#define __natalify__Buffer__

#define CHUNK_SIZE  1024
#define PITCH_BLOCK 1024
#define DB_MIN      -200.0f

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include <memory>
#include <vector>
#include <string>
#include <limits>

#include <sndfile.h>
#include <aubio/aubio.h>
#include "Dirac.h"
#include "easing.h"

using namespace std;

// Globally defined in the main
extern int BPM;
extern int Hz;


// Converts from deciBels to linear volume
float db2l(float db);

// Converts from linear volume to deciBel
float l2db(float linear);

float t1bpm(float sec);
int t2chunks(float sec);
float bpm2t(float beats);
int bpm2chunks(float beats);

void smbPitchShift(float pitchShift, long numSampsToProcess, long fftFrameSize, long osamp, float sampleRate, float *indata, float *outdata);

struct omiChunk
{
    int start;
    int end;
    
    omiChunk() : start(0), end(0) {}
};

struct omiLevel
{
    float db;
    float linear;
    float energy;
    float hfc;
    float pitch;
    
    omiLevel() : db(0), linear(0), energy(0), hfc(0), pitch(0) {}
    omiLevel(float db, float energy, float hfc, float pitch) :
        db(db), energy(energy), hfc(hfc), pitch(pitch) { linear = db2l(db); }
};

typedef vector<omiLevel> omiLevelVec;
typedef omiLevelVec::iterator omiLevelIter;

struct omiCircularBuffer
{
    vector<float> v;
    int index;
    int n;
    
    omiCircularBuffer(int n) : n(n)
    {
        v.reserve(n);
        index = 0;
    }
    
    void add(float f)
    {
        if(v.size() < n)
        {
            v.push_back(f);
            index++;
        }
        else
        {
            v[index] = f;
            index = (index + 1) % n;
        }
    }
    
    float avg()
    {
        float sum = 0;
        vector<float>::iterator i;
        for(i = v.begin(); i < v.end(); i++)
            sum += *i;
        return sum / v.size();
    }
    
    float max()
    {
        float max = -numeric_limits<float>::max();
        vector<float>::iterator i;
        for(i = v.begin(); i < v.end(); i++)
            max = *i > max ? *i : max;
        return max;
    }
    
    float min()
    {
        float min = numeric_limits<float>::max();
        vector<float>::iterator i;
        for(i = v.begin(); i < v.end(); i++)
            min = *i < min ? *i : min;
        return min;
    }
};

class Buffer
{
public:
    static void* diracFx;
    static void* dirac;
    static aubio_pitchdetection_t *pitchDetector;
    
    long len;
    int chunks;
    float *data[1];
    SF_INFO info;
    
    vector<omiLevel> levels; // levels cache, lazy evaluated
    omiLevel statLevMax;
    omiLevel statLevMin;
    omiLevel statLevAvg;
    
    Buffer();
    
    // Initialize with a portion of another buffer.
    // The data is measured in chunks, each of size of CHUNK_SIZE.
    // The data will be copied as well as the info struct.
    // If the end is not specified (-1) means until the end.
    // Start is defaulted to 0.
    // Defaults: start=0, end=-1 (end of buffer)
    Buffer(const Buffer &src, int start = 0, int end = -1);
    
    // Initialize the buffer by loading the specified wav file.
    Buffer(const string &filename);
    
    ~Buffer();
    
    void reset();
    
    float getTime() const { return (float)len / (float)info.samplerate; }
    float getTimeSnap(float beats) const
    {
        float t = getTime();
        float bt = bpm2t(beats);
        float times = roundf(t / bt);
        times = times * bt;
        return max(times, bt);
    }
    
    // Create and empty buffer
    void create(int nhunks, const SF_INFO &baseInfo);
    
    // Copy the content of src into the buffer starting from the index
    // position until the end is reached. If the buffer size is exceeded
    // the rest of the data is not copied.
    // The data is copied in blocks of CHUNK_SIZE as well as the index.
    // Also the levels informations are copied to avoid recomputation and
    // keep the members consistent. If mix is set the data will be mixed
    // with the already data in the buffer.
    // Returns the number of chunks copied.
    int blit(const Buffer &src, int chunkStartIndex, float mix);
    
    // Load the specified wav file from the disk and store the content
    // in the data buffer as float [-1,1] normalized values.
    // Only single channel (mono) audio are supported so far.
    // Returns: 1-success, 0-fail
    bool loadWav(const string &filename);
    
    // Writes the content of the buffer to the specified location.
    // The output format will be the same as the read file, otherwise
    // the format must be specified setting the info member struct.
    // Returns: 1-success, 0-fail
    bool writeWav(const string &filename);
    
    // Apply pitch shift and time streach effects to the buffer.
    // This modifies the buffer permanently.
    // If the time is changed then the buffer size will change,
    // as well as the memory pointers.
    void applyDirac(float timeRatio, float pitchSemitone);
    
    void applyAutotune();
    
    void applyPitches(const vector<float> &pitchSemitone);
    
    // Compute and store a cached vector of n chunks volume and energy values
    // Also statistical values are stored, such as min, max and avg
    void computeLevels();
    
    // Computes only the statistical values of the levels.
    // This is useful when blitting some data from another buffer, so there's
    // no need to calculate the values but only stats update.
    void computeStats();
    
    // Eliminate the silent edges from the buffer.
    // The threshold is in deciBel.
    // Defaults: threshold = 0.1f
    void trim(float threshold = 0.1f);
    
    // Splits the buffer when silence is detected (based on the threshold).
    // The threshold is a percentage value [0,1] based on the sound range.
    // Returns a vector of Buffer.
    // The info struct will be copied from the this.
    // Defaults: threshold = 0.1f
    vector<shared_ptr<Buffer>> splitSilence(float threshold = 0.1f);
    
    vector<shared_ptr<Buffer>> splitEnergy(float threshold = 0.1f);
    
    // Uses the mean pitch to adjust the sound
    void pitchFlat();
    
    // Gain the sound by factor
    void gain(float factor);
    
    void normalize(float threshold_dB);
};

typedef vector<shared_ptr<Buffer>> BufferVec;
typedef BufferVec::iterator BufferIter;

#endif
