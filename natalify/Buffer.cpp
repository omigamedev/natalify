#include <float.h>
#include "Buffer.h"

float db2l(float db)
{
    return powf(10.0f, 0.05f * db);
}

float l2db(float linear)
{
    return 20.0f * log10f(linear);
}

float t1bpm(float sec)
{
    return (float)BPM / (60.0f * sec);
}

int t2chunks(float sec)
{
    return ceilf(sec * (float)Hz / (float)CHUNK_SIZE);
}

float bpm2t(float beats)
{
    return 60.0f * beats / (float)BPM;
}

int bpm2chunks(float beats)
{
    float t = bpm2t(beats);
    return ceilf(t * Hz / (float)CHUNK_SIZE);
}

long dataOffset = 0;
long dataLen = 0;
float *dataPointer = 0;
long myReadData(float **chdata, long numFrames, void *userData)
{
    long len = min(numFrames, dataLen - numFrames);
	
    if (!chdata || len==0)
        return 0;
	
	memcpy(chdata[0], dataPointer + dataOffset, len * sizeof(float));
    dataOffset += len;
    
	return len;
}

// static members
void* Buffer::diracFx = 0;
void* Buffer::dirac = 0;
aubio_pitchdetection_t* Buffer::pitchDetector = 0;

Buffer::Buffer()
{
    data[0] = 0;
    len = 0;
    chunks = 0;
}

Buffer::Buffer(const string &filename)
{
    loadWav(filename);
}

Buffer::Buffer(const Buffer &src, int start, int end)
{
    if(end < 0)
        end = src.chunks;
    
    chunks = end - start;
    len = chunks * CHUNK_SIZE;
    info = src.info;
    
    // copy the data buffer
    data[0] = new float[len];
    memcpy(data[0], src.data[0] + start * CHUNK_SIZE, len * sizeof(float));
    
    // copy the levels data, no need to recalculate
    levels = vector<omiLevel>(chunks);
    for(int i = 0; i < chunks; i++)
        levels[i] = src.levels[start+i];
    
    computeStats();
}

Buffer::~Buffer()
{
    reset();
}

void Buffer::create(int nchunks, const SF_INFO &baseInfo)
{
    info = baseInfo;
    chunks = nchunks;
    len = chunks * CHUNK_SIZE;
    data[0] = new float[len];
    levels = vector<omiLevel>(chunks);
}

void Buffer::reset()
{
    if(data[0])
        delete[] data[0];
    len = 0;
    chunks = 0;
    data[0] = 0;
    levels.clear();
}

bool Buffer::loadWav(const string &filename)
{
    SNDFILE *fp = sf_open(filename.c_str(), SFM_READ, &info);
    if(!fp) return false;
    
    chunks = (int)ceil((float)info.frames / (float)CHUNK_SIZE);
    
    // allocate the buffer, in size of multiple of CHUNK_SIZE
    data[0] = new float[chunks * CHUNK_SIZE];
    len = chunks * CHUNK_SIZE;
    
    long read = sf_readf_float(fp, data[0], info.frames);
    sf_close(fp);
    
    // pad with zeros
    memset(data[0], 0, len-read);
    
    return true;
}

bool Buffer::writeWav(const string &filename)
{
    SF_INFO out_info = info;
    
    SNDFILE *fpout = sf_open(filename.c_str(), SFM_WRITE, &out_info);
    if(!fpout) return false;
    
    long written = sf_writef_float(fpout, data[0], len);
    
    if(written != len)
        printf("written on %s %ld/%ld frames\n", filename.c_str(), written, len);
    
    sf_close(fpout);
    
    return true;
}

void Buffer::applyAutotune()
{
    // lazy initialization
    if(dirac == 0)
        dirac = DiracCreate(kDiracLambdaPreview, kDiracQualityPreview, 1, info.samplerate, myReadData, 0);
    
    dataLen = len;
    dataPointer = data[0];
    
    DiracSetProperty(kDiracPropertyDoPitchCorrection, 1, dirac);
    DiracSetProperty(kDiracPropertyPitchCorrectionSlurTime, 0, dirac);
    
    float *diracOut[1] = {new float[len]};
    long processed = DiracProcess(diracOut, len, dirac);
    
    delete[] data[0];
    data[0] = diracOut[0];
    
    assert(processed);    
}

void Buffer::applyPitches(const vector<float> &pitchSemitone)
{
    smbPitchShift(1, len, CHUNK_SIZE, 64, info.samplerate, data[0], data[0]);
    return;
    
    if(levels.empty())
        computeLevels();
    
    // lazy initialization
    if(diracFx == 0)
        diracFx = DiracFxCreate(kDiracQualityGood, info.samplerate, info.channels);
    
    long latencyLen = DiracFxLatencyFrames(info.samplerate);
    
    float *out[1] = {new float[latencyLen]};
    float *in[1] = {new float[latencyLen]};
    
    for(int i = 1; i < chunks/10; i++)
    {
        printf("%d/%d\n", i, chunks);

//        float pitchRatio = pow(2.0f, sin(i/40.0f)*5/12.0f);
        float pitchRatio = sinf(i/40.0f) * 5 / 12.0f;
        
        //memcpy(in[0], data[0]+i*CHUNK_SIZE, CHUNK_SIZE * sizeof(float));
        //DiracFxProcessFloat(1, pitchRatio, in, out, latencyLen, diracFx);
        //DiracFxProcessFloat(1, pitchRatio, in, out, CHUNK_SIZE, diracFx);
        smbPitchShift(1, CHUNK_SIZE, CHUNK_SIZE, 64, info.samplerate, data[0]+i*CHUNK_SIZE, data[0]+i*CHUNK_SIZE);
        //memcpy(data[0]+i*CHUNK_SIZE, out[0], CHUNK_SIZE * sizeof(float));
    }
    
    delete[] out[0];
    
    computeLevels();
}

void Buffer::applyDirac(float timeRatio, float pitchSemitone)
{
    // applying fx without loading any data?
    assert(data[0] != 0);
    
    // lazy initialization
    if(diracFx == 0)
        diracFx = DiracFxCreate(kDiracQualityGood, info.samplerate, info.channels);
    
    chunks = ceil((len * timeRatio) / (float)CHUNK_SIZE);
    long newLen = chunks * CHUNK_SIZE;
    float pitchRatio = pow(2.0f, pitchSemitone / 12.0f);
    long latencyLen = DiracFxLatencyFrames(info.samplerate);
    
    float *copy = new float[len+latencyLen];
    float *out[1] = {new float[newLen]};
    float *in[1] = {copy+latencyLen};
    
    memcpy(copy, data[0], len*sizeof(float));
    
    long processed;
    DiracFxProcessFloat(timeRatio, pitchRatio, data, out, latencyLen, diracFx);
    processed = DiracFxProcessFloat(timeRatio, pitchRatio, in, out, len, diracFx);
    
    // Dirac didn't process anything, shouldn't happen
    assert(processed > 0);
    
    if(processed != newLen)
        printf("processed != newLen\n");
    
    // delete the old buffer
    delete[] data[0];
    delete[] copy;
    
    // update length and buffer pointer
    len = newLen;
    data[0] = out[0];
    
    // clear any cached value
    levels.clear();
}

void Buffer::computeLevels()
{
    // clear if old values were stored
    if(!levels.empty())
        levels.clear();
    
    // allocate statically
    levels = vector<omiLevel>(chunks);
    
    // lazy initialize aubio
    if(pitchDetector == 0)
        pitchDetector = new_aubio_pitchdetection(PITCH_BLOCK, 32, 1, info.samplerate, aubio_pitch_schmitt, aubio_pitchm_midi);

    float sumLevel = 0;
    float minLevel = numeric_limits<float>::max();
    float maxLevel = -numeric_limits<float>::max();

    float sumEnergy = 0;
    float minEnergy = numeric_limits<float>::max();
    float maxEnergy = -numeric_limits<float>::max();
    
    float sumHFC = 0;
    float minHFC = numeric_limits<float>::max();
    float maxHFC = -numeric_limits<float>::max();
    
    float sumPitch = 0;
    float minPitch = numeric_limits<float>::max();
    float maxPitch = -numeric_limits<float>::max();
    
    for(int i = 0; i < chunks; i++)
    {
        // shifted buffer holder
        float *p[1] = {&data[0][i * CHUNK_SIZE]};
        
        // temp aubio buffer storage that points to the class data
        fvec_t vec;
        vec.length = CHUNK_SIZE;
        vec.channels = 1;
        vec.data = p;
        
        // compute the chunk level with aubio
        float db = aubio_level_detection(&vec, DB_MIN);
        db = db == 1.0f ? DB_MIN : db;
        
        // compute energy values
        float energy = vec_local_energy(&vec);
        float energy_hfc = vec_local_hfc(&vec);

        // statistics
        sumLevel += db;
        minLevel = minLevel < db ? minLevel : db;
        maxLevel = maxLevel > db ? maxLevel : db;
        
        sumEnergy += db;
        minEnergy = minEnergy < energy ? minEnergy : energy;
        maxEnergy = maxEnergy > energy ? maxEnergy : energy;
        
        sumHFC += db;
        minHFC = minHFC < energy_hfc ? minHFC : energy_hfc;
        maxHFC = maxHFC > energy_hfc ? maxHFC : energy_hfc;
        
        float pitch;
        if((i*CHUNK_SIZE) % PITCH_BLOCK == 0)
        {
            long left = len - i*CHUNK_SIZE;
            vec.length = min(PITCH_BLOCK, (int)left);
            if(left < PITCH_BLOCK)
                left = 0;
            pitch = aubio_pitchdetection(pitchDetector, &vec);
            //printf("volume=%f pitch=%f note=%f\n", db, pitch, aubio_freqtomidi(pitch));
        }
        else
        {
            pitch = levels[i-1].pitch;
        }

        sumPitch += pitch;
        minPitch = minPitch < pitch ? minPitch : pitch;
        maxPitch = maxPitch > pitch ? maxPitch : pitch;

        levels[i] = omiLevel(db, energy, energy_hfc, pitch);
    }
    
    statLevMin = omiLevel(minLevel, minEnergy, minHFC, minPitch);
    statLevMax = omiLevel(maxLevel, maxEnergy, maxHFC, maxPitch);
    statLevAvg = omiLevel(sumLevel / chunks, sumEnergy / chunks, sumHFC / chunks, sumPitch / chunks);
}

void Buffer::computeStats()
{
    // nothing to compute
    if(levels.empty()) return;
    
    float sumLevel = 0;
    float minLevel = numeric_limits<float>::max();
    float maxLevel = -numeric_limits<float>::max();
    
    float sumEnergy = 0;
    float minEnergy = numeric_limits<float>::max();
    float maxEnergy = -numeric_limits<float>::max();
    
    float sumHFC = 0;
    float minHFC = numeric_limits<float>::max();
    float maxHFC = -numeric_limits<float>::max();
    
    float sumPitch = 0;
    float minPitch = numeric_limits<float>::max();
    float maxPitch = -numeric_limits<float>::max();
    
    for(int i = 0; i < chunks; i++)
    {
        float db = levels[i].db;
        float energy = levels[i].energy;
        float energy_hfc = levels[i].hfc;
        float pitch = levels[i].pitch;
        
        sumLevel += db;
        minLevel = minLevel < db ? minLevel : db;
        maxLevel = maxLevel > db ? maxLevel : db;
        
        sumEnergy += energy;
        minEnergy = minEnergy < energy ? minEnergy : energy;
        maxEnergy = maxEnergy > energy ? maxEnergy : energy;
        
        sumHFC += energy_hfc;
        minHFC = minHFC < energy_hfc ? minHFC : energy_hfc;
        maxHFC = maxHFC > energy_hfc ? maxHFC : energy_hfc;
        
        sumPitch += pitch;
        minPitch = minPitch < pitch ? minPitch : pitch;
        maxPitch = maxPitch > pitch ? maxPitch : pitch;
    }
    
    statLevMin = omiLevel(minLevel, minEnergy, minHFC, minPitch);
    statLevMax = omiLevel(maxLevel, maxEnergy, maxHFC, maxPitch);
    statLevAvg = omiLevel(sumLevel / chunks, sumEnergy / chunks, sumHFC / chunks, sumPitch / chunks);
}

void Buffer::trim(float threshold)
{
    // TODO: check if the buffer has data

    // compute volume levels if not already in the cache
    if(levels.empty())
        computeLevels();
    
    // compute level from percentage
    float thrLevel = statLevMin.linear + (statLevMax.linear - statLevMin.linear) * threshold;
    
    long start = 0;
    long end = levels.size() - 1;
    
    int tail = 4;
    
    // move forward the start index
    while(start < end)
    {
        if(levels[start].linear > thrLevel)
        {
            int imax = (int)(levels.size() - start);
            int i = 0;
            while(i < tail+1 && i < imax && levels[start+i].linear > thrLevel)
                i++;
            if(i == tail+1)
                break;
        }
        start++;
    }
    
    // move backward the end index
    while(start < end)
    {
        if(levels[end].linear > thrLevel)
        {
            int i = 1;
            while(i < tail+1 && i > 0 && levels[end-i].linear > thrLevel)
                i++;
            if(i == tail+1)
                break;
        }
        end--;
    }
    
    long newLen = (end-start) * CHUNK_SIZE;
    
    // store the result
    if(newLen == 0)
    {
        // the buffer was completely silent, free everything
        len = 0;
        chunks = 0;
        if(data[0])
            delete[] data[0];
        data[0] = 0;
        levels.clear();
    }
    else
    {
        // keep only the a subset of levels
        vector<omiLevel> tmp = vector<omiLevel>(levels.begin()+start, levels.begin()+end);
        levels = tmp;
        
        // create a copy of the data
        float *newData = new float[newLen];
        memcpy(newData, data[0]+start*CHUNK_SIZE, newLen * sizeof(float));
        // delete the old data and subsitute
        if(data[0])
            delete[] data[0];
        data[0] = newData;
        
        // update members
        len = newLen;
        chunks = (int)levels.size();
    }
}

BufferVec Buffer::splitSilence(float threshold)
{
    BufferVec ret;
    
    // trim(threshold);
    if(levels.empty())
        computeLevels();

    // compute level from percentage
    float thrLevel = statLevMin.linear + (statLevMax.linear - statLevMin.linear) * threshold;

    // TODO: check if the buffer has data
    
    int trigger = 6;
    int minBlockSize = 10;
    
    int group = -1;
    int groupStart = 0;
    int diffStart = 0;
    int diffCount = 0;
    int idx = 0;
    
    while(idx < chunks)
    {
        // TODO: try with hysteresis
        int silence = levels[idx].linear < thrLevel;
        
        // first cycle
        if(group == -1)
        {
            group = silence;
            groupStart = idx;
        }
        
        if(silence != group)
        {
            // first time
            if(diffStart == -1)
                diffStart = idx;
            
            diffCount++;
            if(diffCount > trigger)
            {
                if(group == 0 && (diffStart-groupStart) >= minBlockSize)
                {
                    // the edges are expanded by 1, the max assures no out of bound in vector
//                    printf("group from %05d to %05d silence = %d\n", groupStart, diffStart-1, group);
                    shared_ptr<Buffer> b(new Buffer(*this, max(0, groupStart-4), diffStart+3));
                    ret.push_back(b);
                }
//                else
//                    printf(" no group from %05d to %05d silence = %d\n", groupStart, diffStart-1, group);

                group = silence;
                groupStart = diffStart;
            }
        }
        else
        {
            diffStart = -1;
            diffCount = 0;
        }
        
        idx++;
    }
    
    // last group terminated by the end of the buffer
    if(group == 0 && (chunks-1 - groupStart) >= minBlockSize)
    {
        // the edges are expanded by 1, the max assures no out of bound in vector
//        printf("group from %05d to %05d silence = %d\n", groupStart, chunks-1, group);
        shared_ptr<Buffer> b(new Buffer(*this, max(0, groupStart-1), chunks));
        ret.push_back(b);
    }
//    else
//        printf(" no group from %05d to %05d silence = %d\n", groupStart, chunks-1, group);
    
    return ret;
}

BufferVec Buffer::splitEnergy(float threshold)
{
    BufferVec ret;
    
    // compute level from percentage
    float thrLevel = statLevMin.energy + (statLevMax.energy - statLevMin.energy) * threshold;
    
    // TODO: check if the buffer has data
    
    int idx = 0;
    int nlouds = 0;
    int start = 0;
    
    int minChunks = (0.25 * info.samplerate) / CHUNK_SIZE;
    
    while(idx < chunks-5)
    {
        int silence = levels[idx].energy < thrLevel;
        
        if(silence && nlouds > minChunks && levels[idx+1].energy > thrLevel)
        {
            int count = 0;
            for(int i = 0; i < 6; i++)
                if(levels[idx+1+i].energy > thrLevel)
                    count++;
            if(count > 4)
            {
                printf("cut at %d\n", idx);
                shared_ptr<Buffer> b(new Buffer(*this, start, idx-1));
                ret.push_back(b);
                start = idx;
                nlouds = 0;
            }
            else
            {
                nlouds++;
            }
        }
        else
        {
            nlouds++;
        }
        
        idx++;
    }
    
    if(nlouds > 4)
    {
        shared_ptr<Buffer> b(new Buffer(*this, start, chunks-1));
        ret.push_back(b);
    }
    
    return ret;
}

int Buffer::blit(const Buffer &src, int chunkStartIndex, float mix)
{
    // index out of bounds
    if(chunkStartIndex >= chunks)
        return 0;
    
    if(levels.empty())
        computeStats();
    
    int count = min(chunks-chunkStartIndex, src.chunks);
    
    // copy data
    if(mix == 1.0f)
    {
        memcpy(data[0]+chunkStartIndex*CHUNK_SIZE, src.data[0], count * CHUNK_SIZE * sizeof(float));
    }
    else
    {
        for(int i = 0; i < count * CHUNK_SIZE; i++)
        {
            float *p = data[0] + chunkStartIndex * CHUNK_SIZE + i;
//            *p = *p * (1.0f-mix) + src.data[0][i] * mix;
            *p = *p + src.data[0][i];
        }
    }
    
    // clear levels
    levels.clear();
    
    return count;
}

void Buffer::pitchFlat()
{
    if(levels.empty())
        computeLevels();

    // lazy initialization
    if(diracFx == 0)
        diracFx = DiracFxCreate(kDiracQualityGood, info.samplerate, info.channels);
    
    long latencyLen = DiracFxLatencyFrames(info.samplerate);

    float *out[1] = {new float[latencyLen]};
    float *in[1] = {new float[latencyLen]};

    for(int i = 1; i < chunks; i++)
    {
        int target = 50 % 12;
        int relative = (int)levels[i].pitch % 12;
        float p = (target - levels[i].pitch) / 12.0f;
        float pitchRatio = pow(2.0f, p);
        
        memcpy(in[0], data[0]+i*CHUNK_SIZE, CHUNK_SIZE * sizeof(float));
        DiracFxProcessFloat(1, pitchRatio, in, out, latencyLen, diracFx);
        DiracFxProcessFloat(1, pitchRatio, in, out, CHUNK_SIZE, diracFx);
        memcpy(data[0]+i*CHUNK_SIZE, out[0], CHUNK_SIZE * sizeof(float));
    }
    
    delete[] out[0];
    
    computeLevels();
}

void Buffer::gain(float factor)
{
    printf("gain %f\n", factor);
    for(int i = 0; i < len; i++)
        data[0][i] *= factor;
    levels.clear();
}

void Buffer::normalize(float threshold_dB)
{
    if(levels.empty())
        computeLevels();

    float sum = 0;
    long n = 0;
    for(int k = 0; k < len; k++)
    {
        if(data[0][k] > 0.01)
        {
            sum += data[0][k];
            n++;
        }
    }
    
    if(n == 0)
        return;
    
    sum /= n;

    for(int i = 0; i < levels.size(); i++)
    {
        if(levels[i].db < -100)
            continue;

        float factor = 0.2 / sum;
        for(int k = 0; k < CHUNK_SIZE; k++)
        {
            data[0][i*CHUNK_SIZE+k] *= factor;
        }
    }
    levels.clear();
}
