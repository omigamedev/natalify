#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include <fftw3.h>

#include <vector>
#include <string>

#include "Buffer.h"

using namespace std;

// Globally used
int BPM = 100;
int Hz  = 44100;

void saveParts(BufferVec &parts)
{
    int i = 0;
    for(BufferIter iter = parts.begin(); iter != parts.end(); iter++)
    {
        Buffer &b = **iter;
        char str[256];
        sprintf(str, "out_%02d.wav", i++);
        b.writeWav(str);
    }
}

Buffer input, track, music;
int offset = 0;

void nextBlock(float beats)
{
    int qchunks = bpm2chunks(beats);
    int qi = floor((float)offset / (float)qchunks) + 1;
    offset = qi * qchunks;
}

void pause(int time)
{
    offset += bpm2chunks(time);
}

void put(const Buffer &b)
{
    static int putPieces = 0;
    
    Buffer tmp(b);
//    tmp.computeLevels();
//    tmp.pitchFlat();
//    tmp.applyDirac(time, note);
    track.blit(tmp, offset-3, 0.5);
    offset += tmp.chunks;
    if(putPieces == 7)
    {
        //pause(1);
        putPieces = 0;
    }
    putPieces++;
}

void putSnapped(const BufferVec &vec)
{
    for(int i = 0; i < vec.size(); i++)
    {
        printf("time %f snap %f\n", vec[i]->getTime(), vec[i]->getTimeSnap(0.5));
        
        float ratio = vec[i]->getTimeSnap(0.5) / vec[i]->getTime();
        
        vec[i]->applyDirac(ratio, 0);
        vec[i]->computeLevels();
        
        printf("time %f snap %f\n", vec[i]->getTime(), vec[i]->getTimeSnap(0.5));
        
        int n = (int)(vec[i]->getTimeSnap(0.5) / 0.25f);
        int chunkSize = vec[i]->chunks / n;
        printf("pieces %d\n", n);
        
        for(int k = 0; k < n; k++)
        {
            int start = k*chunkSize;
            int end = k < n-1 ? (k+1)*chunkSize : vec[i]->chunks; //TODO: -1 or not?
            Buffer p(*vec[i], start, end);
            put(p);
        }
    }
}

void graph(float *v, int n)
{
    FILE *fp = fopen("out-graph.tmp", "w");
    assert(fp);
    for(int i = 0; i < n; i++)
        fprintf(fp, "%f\n", v[i]);
    fclose(fp);
    system("/opt/local/bin/gnuplot -e 'plot \"out-graph.tmp\" w lines'");
}

int main(int argc, const char * argv[])
{
    srand((int)time(0));
    
#ifdef __MACH__
//    system("/opt/local/bin/sox --norm=5 data/lalla.wav input.wav");
#endif
    input.loadWav("data/auguri.wav");
    music.loadWav("data/heart-of-the-sea.wav");
    track.create(t2chunks(15), input.info);
    
    input.gain(1.5);
//    input.normalize(-50.0f);
//    input.computeLevels();
//    input.trim(0.01f);
//    input.applyDirac(1, 1);
//    input.pitchFlat();
    input.writeWav("out.wav");
//    return 0;
    
    track.gain(0.5);
//    input.gain(1.5);
    
#ifdef __MACH__
    {
        FILE *fp_db = fopen("out-graph-db.tmp", "w");
        FILE *fp_lin = fopen("out-graph-lin.tmp", "w");
        FILE *fp_erg = fopen("out-graph-erg.tmp", "w");
        for(int i = 0; i < input.levels.size(); i++)
        {
            fprintf(fp_db, "%f\n", input.levels[i].db-input.statLevMin.db);
            fprintf(fp_lin, "%f\n", input.levels[i].linear*10000);
            fprintf(fp_erg, "%f\n", input.levels[i].energy);
        }
        fclose(fp_db);
        fclose(fp_lin);
        fclose(fp_erg);
        system("/opt/local/bin/gnuplot -e 'plot \"out-graph-db.tmp\" w lines, \"out-graph-lin.tmp\" w lines, \"out-graph-erg.tmp\" w lines'");
    }
#endif
    
    BufferVec parts = input.splitSilence(0.01f);
    saveParts(parts);
    
    nextBlock(0.5);
    for(int k = 0; k < 5; k++)
    {
        for(int i = 0; i < parts.size(); i++)
        {
            Buffer &a = *parts[i];
            a.computeLevels();
            BufferVec v = a.splitEnergy(0.01);
            putSnapped(v);
//            putSnapped(parts);
            nextBlock(4);
        }
        nextBlock(4);
    }
    
//    track.create((10*44100)/CHUNK_SIZE, input.info);
//    put(a, 0, 1);
//    put(a, 7, 1);
//    put(a, 9, 1);
//    put(a, 7, 2);
//    put(b, 7, 3);
//    put(a, 7, 1);
//    put(a, 9, 1);
//    put(a, 5, 4);
//    put(b, 0, 8);
    
//    input.applyDirac(1, 1);
//    input.writeWav("out.wav");
//    b.writeWav("out_b.wav");
    vector<float> pitches;
    track.applyPitches(pitches);
    track.computeLevels();
    track.writeWav("out_voice.wav");
    
    music.blit(track, 0, 0.5);
//    music.blit(track, 10*44100/CHUNK_SIZE, 0.5);
    music.writeWav("out_mix.wav");
    
#ifdef __MACH__
    system("/opt/local/bin/gnuplot -e 'plot \"out.txt\" w lines'");
//    system("open -a /Applications/VLC.app out_voice.wav");
#endif
    return 0;
}

